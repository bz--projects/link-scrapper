#!/usr/bin/env node

//Grab provided args.
const [, , ...args] = process.argv;

require("../scrapers/film2movie")(
  args[0], // pages
  args[1] // path
);
