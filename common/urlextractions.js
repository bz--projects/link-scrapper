var getseasonepisode = link => {
  link = link.toLowerCase();
  result = link.match(/\.s\d{1,2}\.|\.s\d{1,2}e\d{1,2}\./);
  result = result && result[0].replace(/\.|s/g, "").split("e");
  return {
    season: result ? parseInt(result[0]) : null,
    episode: result && result[1] ? parseInt(result[1]) : null,
  };
};

var getquality = link => {
  link = link.toLowerCase();

  return {
    quality: link.match(/hdcam|360p|480p|720p|1080p/)
      ? link.match(/hdcam|360p|480p|720p|1080p/)[0]
      : "",
    x265: link.match(/x265/) ? true : false,
  };
};

var gettype = link => {
  link = link.toLowerCase();
  return { type: lowerlink.includes(".trailer.") ? "trailer" : "main" };
};

var getimdbid = link => {
  link = link.toLowerCase();
  if (link.match(/imdb\.com\/title\/?.+/)) {
    return link.match(/\/tt?[0-9]+\//)[0].replace(/\//g, "");
  }
  return null;
};

module.exports = {
  getseasonepisode: getseasonepisode,
  getquality: getquality,
  gettype: gettype,
  getimdbid: getimdbid,
};
