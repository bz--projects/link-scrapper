// extracts film2movie movies and return promise (array of object)
//  if path is defeind write them to coresponding folder

const axios = require("axios");
var fs = require("fs");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const urlexts = require("../common/urlextractions");

var validformats = [".mkv", ".mp4", ".avi"];

module.exports = function(pages = "1-5", p) {
  return new Promise((mres, mrej) => {
    let url = "http://www.film2movie.us/page/",
      finalresult = [],
      lastpagenum = null;
    pages = pages.toString().split("-");
    pages[1] = pages[1] || pages[0];
    if (p) {
      path = p.slice(-1) == "/" ? p : p + "/";
      path = path + "film2movie_" + Date.now() + "/";
      fs.mkdirSync(path);
    } else {
      path = null;
    }
    for (let page = pages[0]; page <= pages[1]; page++) {
      if (lastpagenum && page > lastpagenum) {
        break;
      }
      axios({
        method: "get",
        url: url + page,
      })
        .then(function(resp) {
          const dom = new JSDOM(resp.data);
          document = dom.window.document;
          if ((page = pages[0])) {
            lastpagenum = parseInt(
              document.getElementById("wbh-pagenumber").lastChild.innerText
            );
          }

          let links = document.querySelectorAll("a.more-link");
          new Promise((res, ref) => {
            for (let i = 0; i < links.length; i++) {
              film2moviescrap(links[i].href, path).then(data => {
                finalresult.push(data);
                if (finalresult.length == links.length && page == pages[1])
                  res(finalresult);
              });
            }
          }).then(
            data => {
              mres(data);
            },
            rej => {
              mres(null);
            }
          );
        })
        .catch(function(resp) {
          console.log(resp);
        });
    }
  });
};

function film2moviescrap(url, path) {
  return new Promise((res, rej) => {
    axios({ method: "get", url: url })
      .then(function(resp) {
        const dom = new JSDOM(resp.data);
        document = dom.window.document;
        links = document.getElementsByTagName("a");
        for (i = 0; i < links.length; i++) {
          if ((imdb_id = urlexts.getimdbid(links[i].href))) break;
        }
        if (!imdb_id) res({});
        movie = {
          imdb_id: imdb_id,
          site_url: "http://www.film2movie.co",
          links: [],
        };

        for (i = 0; i < links.length; i++) {
          if (validformats.indexOf(links[i].href.slice(-4)) >= 0) {
            lowerlink = links[i].href.toLowerCase();
            let se = urlexts.getseasonepisode(lowerlink);
            let quality = urlexts.getquality(lowerlink);
            let type = urlexts.gettype(lowerlink);
            movie.links.push({
              imdb_id: imdb_id,
              link_url: links[i].href,
              extraction_url: url,
              ...type,
              ...se,
              ...quality,
            });
          }
        }
        if (path) {
          fs.writeFile(
            path + imdb_id + ".json",
            JSON.stringify(movie),
            function() {
              res(movie);
            }
          );
        } else {
          res(movie);
        }
      })
      .catch(function(resp) {
        res({});
      });
  });
}
