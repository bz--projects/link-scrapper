// extracts film2movie movies and return promise (array of object)
//  if path is defeind write them to coresponding folder

const axios = require("axios");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

var videoformats = [".mkv", ".mp4", ".avi"];

module.exports = function(title) {
  return new Promise((mres, mrej) => {
    let url = "https://www.imdb.com/title/";
    movieinfo = {};
    axios({
      method: "get",
      url: url + title,
    })
      .then(function(resp) {
        const dom = new JSDOM(resp.data);
        document = dom.window.document;

        movieinfo = {
          title: gettitle(document),
          dates: getdates(document),
          certrate: getcertrate(document),
          duration: getduration(document),
          genre: getgenre(document),
          imdbrate: getimdbrate(document),
          imdbratecount: getimdbratecount(document),
          description: getdescription(document),
          country: getcountries(document),
          poster: null,
          videos: [],
          photos: [],
        };
        getvideos().then(data => {
          movieinfo.trailers = data;
          if (movieinfo.photos && movieinfo.poster && movieinfo.trailers) {
            mres(movieinfo);
          }
        });
        getphotos().then(data => {
          movieinfo.photos = data;
          if (movieinfo.photos && movieinfo.poster && movieinfo.trailers) {
            mres(movieinfo);
          }
        });
        getposter().then(data => {
          movieinfo.poster = data;
          if (movieinfo.photos && movieinfo.poster && movieinfo.trailers) {
            mres(movieinfo);
          }
        });
      })
      .catch(function(resp) {
        console.log(resp);
      });
  });
};

var gettitle = document => {
  return document
    .getElementsByTagName("h1")[0]
    .innerHTML.replace(/<.*>|&nbsp;/g, "")
    .trim();
};
var getdates = document => {
  let date = document.querySelector(
    ".title_wrapper .subtext [title='See more release dates']"
  ).innerText;
  ed = null;
  rd = new Date(date);
  if (rd == "Invalid Date") {
    rd = date.match(/\d{4}/g);
    if (rd.length == 2) {
      ed = new Date(rd[1]);
    }
    rd = new Date(rd[0]);
  }
  return { releasedate: rd, enddate: ed };
};
var getcertrate = document => {
  return document
    .querySelector(".title_wrapper .subtext")
    .innerHTML.replace(/\n/g, "")
    .replace(/<.*>/g, "")
    .trim();
};
var getduration = document => {
  return document
    .querySelector(".title_wrapper .subtext time")
    .innerText.trim();
};
var getgenre = document => {
  ele = document.querySelector(".title_wrapper .subtext a:first-of-type");
  genre = [];
  do {
    genre.push(ele.innerText.trim());
    ele = ele.nextElementSibling;
  } while (!ele.classList.contains("ghost"));
  return genre;
};
var getimdbrate = document => {
  return parseFloat(
    document.querySelector("[itemprop='ratingValue']").innerText.trim()
  );
};
var getimdbratecount = document => {
  return parseInt(
    document
      .querySelector("[itemprop='ratingCount']")
      .innerText.replace(/,/g, "")
      .trim()
  );
};
var getdescription = document => {
  return document
    .querySelector("#titleStoryLine .canwrap span")
    .innerText.trim();
};
var getcountries = document => {
  coun = document
    .querySelector("#titleDetails .txt-block")
    .querySelectorAll("a");
  result = [];
  for (i = 0; i < coun.length; i++) {
    result.push(coun[i].innerText.trim());
  }
  return result;
};
var getposter = document => {
  return new Promise((res, rej) => {
    result = {};
    posterdiv = document.querySelector(".poster");
    result.thumbnail = posterdiv.querySelector("img").src.trim();

    res([]);
  });
};
var getvideos = document => {
  return new Promise((res, rej) => {
    videosllink = document.querySelector(
      "#titleVideoStrip .combined-see-more.see-more a"
    ).href;
    axios({ method: "get", url: originallink }).then(function(resp) {
      const subdom = new JSDOM(resp.data);
      let subdocument = subdom.window.document;
    });
    res([]);
  });
};
var getphotos = document => {
  return new Promise((res, rej) => {
    res([]);
  });
};
